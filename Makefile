.PHONY: help
.DEFAULT_GOAL := help

SSH_USER?=${shell whoami}


ANSIBLE_OPTIONS_HOST=  -e "deployment_host=${deployment_host}"


java-install-centos: ## java-install-centos

	ansible-playbook  --become-user=root ./playbooks/java-install.yaml    	
	
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo -e "Arguments/env variables: \n \
				"